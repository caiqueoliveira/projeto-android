package com.example.compras;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
public class Cadastro_Activity extends Activity{
	 private String jsonResult;
	 private String url;

	 public void sucesso(){
		 Toast.makeText(getApplicationContext(),
			      "Cadastro efetuado com sucesso.", Toast.LENGTH_LONG).show();
			finish();

		}
		
		public void erro(){
			Toast.makeText(getApplicationContext(),
				      "Erro no cadastro. Possivelmente digitado um email j� utilizado.", Toast.LENGTH_LONG).show();
		}
	 
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro);
		
		final EditText textEmail = (EditText)findViewById(R.id.textEmail);
		textEmail.setTextColor(Color.BLUE);
		final EditText textSenha = (EditText)findViewById(R.id.textSenha);
		textSenha.setTextColor(Color.BLUE);
		final EditText textConfirmar = (EditText)findViewById(R.id.textConfirmar);
		textConfirmar.setTextColor(Color.BLUE);		
		
		CheckBox checkTermos = (CheckBox)findViewById(R.id.checkTermos);
		
		Button botaoConfirmar = (Button) findViewById(R.id.botao_confirmar);
		
		botaoConfirmar.setOnClickListener ( new OnClickListener () {	
		
			public void onClick ( View v) {
				String email = textEmail.getText().toString();
				String senha = textSenha.getText().toString();
				String confirmar = textConfirmar.getText().toString();
				
				if (email.length() < 5 || senha.length() <5){
					Toast.makeText(getApplicationContext(),
						      "Dados incompletos. Poucos caracteres digitados. Min 5 caracteres.", Toast.LENGTH_LONG).show();
				}else if(!senha.equals(confirmar)){
					Toast.makeText(getApplicationContext(),
						      "Senhas n�o conferem.", Toast.LENGTH_LONG).show();
				}else{
					url = "http://192.168.25.27/cadastrousuario.php?";
					url = url + "e="+email+"&s="+senha;
					accessWebService();				
					
				}
			
				 
			}
		});
	}
	
	private class JsonReadTask extends AsyncTask<String, Void, String> {
		  @Override
		  protected String doInBackground(String... params) {
		   HttpClient httpclient = new DefaultHttpClient();
		   HttpPost httppost = new HttpPost(url);
		   
		   try {
			
		    HttpResponse response = httpclient.execute(httppost);
		    jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
			 
		   } catch (ClientProtocolException e) {
		    e.printStackTrace();
		   } catch (IOException e) {
		    e.printStackTrace();
		   }
		   return null;
		  }
		 
		  
		  private StringBuilder inputStreamToString(InputStream is) {
		   String rLine = "";
		   StringBuilder answer = new StringBuilder();
		   BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		 
		   try {
		    while ((rLine = rd.readLine()) != null) {
		     answer.append(rLine);
		    }
		   }
		 
		   catch (IOException e) {
		    // e.printStackTrace();
		    Toast.makeText(getApplicationContext(),
		      "Error..." + e.toString(), Toast.LENGTH_LONG).show();
		   }
		   return answer;
		  }
		 
		  @Override
		  protected void onPostExecute(String result) {
			  if(jsonResult.equals("ok")){
				  sucesso();
			  }else{
				  erro();
			  }
		  }
		 }
	 public void  accessWebService() {
		  JsonReadTask task = new JsonReadTask();
		  // passes values for the urls string array
		  task.execute(new String[] { url });
		 }
	
	
}
