package com.example.compras;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
 
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
 
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

 
public class DetalhesProduto_Activity extends Activity {
 private String jsonResult;
 private String url = "http://192.168.25.27/detalhesproduto.php?id=";
 private TextView textNome;
 private TextView textDescricao;
 private TextView textValor;
 private TextView textCategoria;
 private TextView textRespostaQuantidade;
 private SeekBar  seekQuantidade;
 private String id;
 static String nome = "";
 static String valor = "";


 MySingleton carrinho = MySingleton.getInstance();  
 @Override
 protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  setContentView(R.layout.activity_detalhesproduto);
  textNome = (TextView) findViewById(R.id.text_nome);
  textDescricao = (TextView) findViewById(R.id.text_descricao);
  textValor = (TextView) findViewById(R.id.text_valor);
  textCategoria = (TextView) findViewById(R.id.text_categoria);
  textRespostaQuantidade = (TextView) findViewById(R.id.text_respostaquantidade);
  seekQuantidade = (SeekBar) findViewById(R.id.seek_quantidade);
 
  
  seekQuantidade.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

	  @Override 
	   public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { 
		  textRespostaQuantidade.setText(String.valueOf(progress+1));
		  String valorAtual = valor.replace(",", ".");
		  Double valorDouble = Double.parseDouble(valorAtual);
		  String valorStringAlterado = String.valueOf((1+seekQuantidade.getProgress())*valorDouble);
		  textValor.setText(valorStringAlterado.replace(".", ","));
	  } 
	  
	  @Override 
	   public void onStartTrackingTouch(SeekBar seekBar) { 
	    // TODO Auto-generated method stub 
	   } 

	   @Override 
	   public void onStopTrackingTouch(SeekBar seekBar) { 
	    // TODO Auto-generated method stub 
	   } 	  
	  
  });
  
  Bundle extras = getIntent().getExtras();
  if (extras != null) {
      id = extras.getString("id");
  }
  
  url = url+id;
  
  accessWebService();
 }
 
  
 // Async Task to access the web
 private class JsonReadTask extends AsyncTask<String, Void, String> {
  @Override
  protected String doInBackground(String... params) {
   HttpClient httpclient = new DefaultHttpClient();
   HttpPost httppost = new HttpPost(url);
   
   try {
	
    HttpResponse response = httpclient.execute(httppost);
    jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
	 
   } catch (ClientProtocolException e) {
    e.printStackTrace();
   } catch (IOException e) {
    e.printStackTrace();
   }
   return null;
  }
 
  
  private StringBuilder inputStreamToString(InputStream is) {
   String rLine = "";
   StringBuilder answer = new StringBuilder();
   BufferedReader rd = new BufferedReader(new InputStreamReader(is));
 
   try {
    while ((rLine = rd.readLine()) != null) {
     answer.append(rLine);
    }
   }
 
   catch (IOException e) {
    // e.printStackTrace();
    Toast.makeText(getApplicationContext(),
      "Error..." + e.toString(), Toast.LENGTH_LONG).show();
   }
   return answer;
  }
 
  @Override
  protected void onPostExecute(String result) {
   pegarDadosProduto();
  }
 }// end async task
 
 public void accessWebService() {
  JsonReadTask task = new JsonReadTask();
  // passes values for the urls string array
  task.execute(new String[] { url });
 }
 
 // build hash set for list view
 public void pegarDadosProduto() {
  
  try {
   JSONObject jsonResponse = new JSONObject(jsonResult);
   JSONArray jsonMainNode = jsonResponse.optJSONArray("produto");
 
   
    JSONObject jsonChildNode = jsonMainNode.getJSONObject(0);
    nome = jsonChildNode.optString("nome");
    String descricao = jsonChildNode.optString("descricao");
    valor = jsonChildNode.optString("valor");
    String categoria = jsonChildNode.optString("categoria");
    String estoque = jsonChildNode.optString("estoque");
    
    textNome.setText(nome);
    textDescricao.setText(descricao);
    textValor.setText(valor);
    textCategoria.setText(categoria);
    
    int estoqueInt = Integer.parseInt(estoque);
    
    if (estoqueInt < 9){
    	seekQuantidade.setMax(estoqueInt-1);
    }else{
    	seekQuantidade.setMax(9);    	
    }
   
    Button addCart = (Button) findViewById(R.id.addCartButton);
	addCart.setOnClickListener ( new OnClickListener () {	
	
		public void onClick ( View v) {
			carrinho.adicionarAoCarrinho(new ItemComprado(id, seekQuantidade.getProgress()+1, nome, textValor.getText().toString()));
			Toast.makeText(getApplicationContext(), "Item Adicionado ao Carrinho!",
				     Toast.LENGTH_SHORT).show();
			finish();
			
			
		}
	});
    
    
  } catch (JSONException e) {
   Toast.makeText(getApplicationContext(), "Error" + e.toString(),
     Toast.LENGTH_SHORT).show();
  }
 
 }
}