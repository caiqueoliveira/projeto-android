package com.example.compras;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login_Activity extends Activity{
	 private String jsonResult;
	 private String url;
	 private String usuario;
	 private String senha;
	 
	MySingleton carrinho = MySingleton.getInstance();  

	 
	public void autenticado(){
		carrinho.setUsuario(usuario);
		Intent intent = new Intent (Login_Activity.this , MainMenu_Activity.class);
		startActivity (intent);
		finish();
	}
	
	public void naoAutenticado(){
		Toast.makeText(getApplicationContext(),
			      "Email e/ou senha incorretos.", Toast.LENGTH_LONG).show();
	}
	 
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		final EditText textUser = (EditText)findViewById(R.id.campo_User);
		final EditText textSenha = (EditText)findViewById(R.id.campo_Senha);
		
		Button botaoLogin = (Button) findViewById(R.id.botao_logar);
		Button botaoCadastro = (Button) findViewById(R.id.botao_cadastro);

		botaoLogin.setOnClickListener ( new OnClickListener () {	
		
			public void onClick ( View v) {
				usuario = textUser.getText().toString();
				senha = textSenha.getText().toString();
				url = "http://192.168.25.27/verificarlogin.php?";
				url = url + "e="+usuario+"&s="+senha;
				accessWebService();
				 
			}
		});
		
		botaoCadastro.setOnClickListener ( new OnClickListener () {	
			
			public void onClick ( View v) {
				
				Intent intent = new Intent (Login_Activity.this , Cadastro_Activity.class);
				startActivity (intent);
			}		
		});
		
	}
	

	private class JsonReadTask extends AsyncTask<String, Void, String> {
		  @Override
		  protected String doInBackground(String... params) {
		   HttpClient httpclient = new DefaultHttpClient();
		   HttpPost httppost = new HttpPost(url);
		   
		   try {
			
		    HttpResponse response = httpclient.execute(httppost);
		    jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
			 
		   } catch (ClientProtocolException e) {
		    e.printStackTrace();
		   } catch (IOException e) {
		    e.printStackTrace();
		   }
		   return null;
		  }
		 
		  
		  private StringBuilder inputStreamToString(InputStream is) {
		   String rLine = "";
		   StringBuilder answer = new StringBuilder();
		   BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		 
		   try {
		    while ((rLine = rd.readLine()) != null) {
		     answer.append(rLine);
		    }
		   }
		 
		   catch (IOException e) {
		    // e.printStackTrace();
		    Toast.makeText(getApplicationContext(),
		      "Error..." + e.toString(), Toast.LENGTH_LONG).show();
		   }
		   return answer;
		  }
		 
		  @Override
		  protected void onPostExecute(String result) {
			  if(jsonResult.equals("ok")){
				  autenticado();
			  }else{
				  naoAutenticado();
			  }
		  }
		 }
	 public void  accessWebService() {
		  JsonReadTask task = new JsonReadTask();
		  // passes values for the urls string array
		  task.execute(new String[] { url });
		 }
}