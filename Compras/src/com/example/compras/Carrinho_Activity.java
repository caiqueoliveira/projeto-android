package com.example.compras;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
 
public class Carrinho_Activity extends Activity {
	private String jsonResult;
	private String url;
	private String compra;
	
	MySingleton carrinho = MySingleton.getInstance();  
	 private ListView listView;
	 private TextView valor;
	 private String usuario = carrinho.getUsuario();
	 
	 public void confirmado(){
		 Toast.makeText(getApplicationContext(),
			      "Pedido efetuado com sucesso!", Toast.LENGTH_LONG).show();
		 carrinho.limparCarrinho();
			finish();
		}
		
		public void erroCompra(){
			Toast.makeText(getApplicationContext(),
				      "Ocorreu um erro durante a compra, tente novamente mais tarde.", Toast.LENGTH_LONG).show();
		}
	
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_carrinho);
		  listView = (ListView) findViewById(R.id.listView1);
		  valor = (TextView) findViewById(R.id.textValor);

		  listView.setClickable(true);
		  listView.setLongClickable(true);
		  ArrayAdapter<String> simpleAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,carrinho.arrumarArray());
		  listView.setAdapter(simpleAdapter);
		  valor.setText(String.valueOf("R$ "+carrinho.totalCarrinho()));

		  listView.setOnItemLongClickListener(new OnItemLongClickListener() {

	            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
	                    int pos, long id) {
	                                
	            		  final int posicao = pos;          	
	            	new AlertDialog.Builder(Carrinho_Activity.this)
	                .setIcon(android.R.drawable.ic_dialog_alert)
	                .setTitle("Excluir item")
	                .setMessage("Deseja realmente excluir este item do carrinho?")
	                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {

	                    @Override
	                    public void onClick(DialogInterface dialog, int which) {
	                    	carrinho.retirarDoCarrinho(posicao);
	                    	valor.setText(String.valueOf(carrinho.totalCarrinho()));
	                    	ArrayAdapter<String> simpleAdapter = new ArrayAdapter<String>(Carrinho_Activity.this,android.R.layout.simple_list_item_1,carrinho.arrumarArray());
	              		  	listView.setAdapter(simpleAdapter);
	                    }

	                })
	                .setNegativeButton("N�o", null)
	                .show();
	            	
	                return true;
	            }
	        });
		  
		  Button botaoConfirma = (Button) findViewById(R.id.botao_confirma);

			botaoConfirma.setOnClickListener ( new OnClickListener () {	
			
				public void onClick ( View v) {

					new AlertDialog.Builder(Carrinho_Activity.this)
	                .setIcon(android.R.drawable.ic_dialog_alert)
	                .setTitle("Confirmar Pedido")
	                .setMessage("Deseja realmente confirmar este pedido?")
	                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {

	                    @Override
	                    public void onClick(DialogInterface dialog, int which) {
	                    	if(carrinho.getProdutosNoCarrinho() == 0){
	                    		Toast.makeText(getApplicationContext(),
	                    			      "Adicione algum produto ao carrinho para efetuar o pedido.", Toast.LENGTH_LONG).show();
	                    	}else{
	                    	
		                    	url = "http://192.168.25.27/efetuarCompra.php?";	
		    					url = url + "u="+usuario+"&v="+String.valueOf(carrinho.totalCarrinho())+"&c="+carrinho.compactarCarrinho();
		    					accessWebService();
	                    	}
	                    	
	                    }

	                })
	                .setNegativeButton("N�o", null)
	                .show();
					
				}
			});
		  
	}
    
    private class JsonReadTask extends AsyncTask<String, Void, String> {
		  @Override
		  protected String doInBackground(String... params) {
		   HttpClient httpclient = new DefaultHttpClient();
		   HttpPost httppost = new HttpPost(url);
		   
		   try {
			
		    HttpResponse response = httpclient.execute(httppost);
		    jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
			 
		   } catch (ClientProtocolException e) {
		    e.printStackTrace();
		   } catch (IOException e) {
		    e.printStackTrace();
		   }
		   return null;
		  }
		 
		  
		  private StringBuilder inputStreamToString(InputStream is) {
		   String rLine = "";
		   StringBuilder answer = new StringBuilder();
		   BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		 
		   try {
		    while ((rLine = rd.readLine()) != null) {
		     answer.append(rLine);
		    }
		   }
		 
		   catch (IOException e) {
		    // e.printStackTrace();
		    Toast.makeText(getApplicationContext(),
		      "Error..." + e.toString(), Toast.LENGTH_LONG).show();
		   }
		   return answer;
		  }
		 
		  @Override
		  protected void onPostExecute(String result) {
			  
			  if(jsonResult.equals("ok")){
				confirmado(); 
			  }else{
				erroCompra();
			  }
		  }
		 }
	 public void  accessWebService() {
		  JsonReadTask task = new JsonReadTask();
		  // passes values for the urls string array
		  task.execute(new String[] { url });
		 }
    
}