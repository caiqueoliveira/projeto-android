package com.example.compras;


import java.util.ArrayList;

public class MySingleton {
					
		private static String usuario;
	
	 	private static ItemComprado[] carrinhoVetor = new ItemComprado[100];
	 	
	 	private static int produtosNoCarrinho = 0;
	 	private static MySingleton instance;
	 	
	 	public static MySingleton getInstance () {
	         if ( MySingleton.instance == null ) {
	             MySingleton.instance = new MySingleton();
	         }
	         return MySingleton.instance;
	    }
	 	 
	 	public int getProdutosNoCarrinho(){
	 		return produtosNoCarrinho;
	 	}
	 	
		public String getUsuario() {
			return usuario;
		}


		public void setUsuario(String u) {
			usuario = u;
		}

		
		public void adicionarAoCarrinho(ItemComprado item){
	 		carrinhoVetor[produtosNoCarrinho++] = item;
	 	}
	 	
	 	public void retirarDoCarrinho(int pos){
	 		
	 		carrinhoVetor[pos] = null;
	 		carrinhoVetor[pos] = carrinhoVetor[produtosNoCarrinho-1];
	 		produtosNoCarrinho--;
	 	}
	 	
	 	public void limparCarrinho(){
	 		carrinhoVetor = new ItemComprado[100];
	 		produtosNoCarrinho = 0;
	 	}
	 	
	 	public void imprimirCarrinho(){
	 		for(int i = 0; i < produtosNoCarrinho; i++){
	 			System.out.println(carrinhoVetor[i].id+" "+carrinhoVetor[i].quantidade);
	 		}
	 	}
	 	
	 	public Double totalCarrinho(){
	 		Double total = 0.0;
	 		for(int i = 0; i < produtosNoCarrinho; i++){
	 			Double valorDoItem = Double.parseDouble(carrinhoVetor[i].valor.replace(",","."));
	 			total = total + valorDoItem;
	 		}
	 		
	 		return total;
	 	}
	 	
	 	public ArrayList<String> arrumarArray(){
	 		
	 		ArrayList<String> carrinhoArray = new ArrayList<String>();
	 		
	 		for(int i = 0; i < produtosNoCarrinho; i++){
	 			carrinhoArray.add(carrinhoVetor[i].quantidade+"x - "+carrinhoVetor[i].nome+" - R$ "+carrinhoVetor[i].valor);
	 		}
	 		
	 		return carrinhoArray;
	 	}
	 	
	 	public String compactarCarrinho(){
	 		String saida = "";
	 		for(int i = 0; i < produtosNoCarrinho; i++){
	 			saida = saida + carrinhoVetor[i].id+":" + carrinhoVetor[i].quantidade + ";";
	 		}
	 		
	 		return saida;
	 	}
	 	

	 	
}