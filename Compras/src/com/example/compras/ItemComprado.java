package com.example.compras;

public class ItemComprado {

	public String id;
	public int quantidade;
	public String nome;
	public String valor;
	
	public ItemComprado(String id, int quantidade, String nome, String valor) {
		this.id = id;
		this.quantidade = quantidade;
		this.nome = nome;
		this.valor = valor;
	}
	
	
	
}
