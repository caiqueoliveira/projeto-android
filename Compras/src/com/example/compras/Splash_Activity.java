package com.example.compras;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splash_Activity extends Activity implements Runnable {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_);

		Handler handler = new Handler();
		handler.postDelayed(this, 3000);
		
	}
	
	public void run() {
		startActivity(new Intent(this, Login_Activity.class));
		finish();
	}
}
