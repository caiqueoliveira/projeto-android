package com.example.compras;
 
import com.example.compras.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
 
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
 
public class MainMenu_Activity extends Activity{
	 MySingleton singleton = MySingleton.getInstance();
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mainmenu);

		
		Button botaoProdutos = (Button) findViewById(R.id.button_produtos);
		Button botaoCarrinho = (Button) findViewById(R.id.cartButton);
		Button botaoUltimos = (Button) findViewById(R.id.botaoultimos);
		Button botaoSair = (Button) findViewById(R.id.botao_sair);

		TextView textUsuario = (TextView) findViewById(R.id.textUsuario);

		textUsuario.setText(singleton.getUsuario());
		
		botaoProdutos.setOnClickListener ( new OnClickListener () {	
		
			public void onClick ( View v) {
				Intent intent = new Intent (MainMenu_Activity.this , Produtos_Activity.class);
				startActivity ( intent );					 
			}
		});
		
		botaoCarrinho.setOnClickListener ( new OnClickListener () {	
			
			public void onClick ( View v) {
				Intent intent = new Intent (MainMenu_Activity.this , Carrinho_Activity.class);
				startActivity ( intent );
							 
			}
		});
		
		botaoUltimos.setOnClickListener ( new OnClickListener () {	
			
			public void onClick ( View v) {
				Intent intent = new Intent (MainMenu_Activity.this , UltimosPedidos_Activity.class);
				startActivity ( intent );
			 
			}
		});	
		
		botaoSair.setOnClickListener ( new OnClickListener () {	
			
			public void onClick ( View v) {
				finish();			 
			}
		});
		
		
	}
    
 
}